# silc mechanical repo

This repo contains information to assembly a SILC machine. In this repo you will find:

- BOM
- Access to CAD files
- Fabrication instruction
- Assembly instructions

The [CAD files are here](https://a360.co/3qoCUJI). Is an Autodesk Fusion 360 project, you can download it in a variety of formats, like .step. Every important part is coded with a nomenclature described in [this](https://docs.google.com/spreadsheets/d/1U9PXzDDuMUvrZdeiZyxkotUQlNFWC3K-qEuZg0_KKvM/edit?usp=sharing) file, the BOM is also there. The BOM assumes that you will make all the parts.

Some parts of the machine were:

- 3D printed using an FDM printer, [here is the guide](https://docs.google.com/presentation/d/1iNgw_IDoqFjUxcSFMc1zj3Qd1wzETgR-GpVRq-OWMi0/edit?usp=sharing)
- Cut with a CNC (3 mm polycarbonate sheets)
- Cut with a CNC for PCBs
- Drilled
- Injecting silicone in 3d printed molds


